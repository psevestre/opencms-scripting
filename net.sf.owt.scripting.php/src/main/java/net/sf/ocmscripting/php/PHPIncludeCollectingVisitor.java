/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import java.util.HashSet;
import java.util.Set;

/**
 * PHP include visitor which collects a Set of all included files.
 * <p>
 * 
 */
public class PHPIncludeCollectingVisitor implements PHPIncludeVisitor {

    /**
     * The included files
     */
    private Set includedFiles;

    /**
     * The translator is used to translate/normalize paths before saving.
     */
    private PHPPathTranslator translator;

    /**
     * Constructs a new instance.
     */
    public PHPIncludeCollectingVisitor() {
        initializeMembers();
    }

    /**
     * Constructs a new instance using the given translator.
     * 
     * @param translator
     *            The translator instance
     */
    public PHPIncludeCollectingVisitor(PHPPathTranslator translator) {
        initializeMembers();
        this.translator = translator;
    }

    /**
     * Initialize members to sane defaults
     */
    private void initializeMembers() {
        includedFiles = new HashSet();
    }

    /**
     * @see PHPIncludeVisitor#visitIncludeFile(String)
     */
    public void visitIncludeFile(String file) {
        includedFiles.add(translate(file));
    }

    /**
     * Translates the file path if a translator is active.
     * 
     * @param file
     *            The file path to translate
     * @return The eventually translated path
     */
    private String translate(String file) {
        if (translator != null) {
            return translator.translate(file);
        }
        return file;
    }

    /**
     * @see PHPIncludeVisitor#visitSurroundingText(String)
     */
    public void visitSurroundingText(String text) {
        // NOOP
    }

    /**
     * Returns the collected includes.
     * <p>
     * 
     * @return The set of collected includes
     */
    public Set getIncludedFiles() {
        return includedFiles;
    }

}
