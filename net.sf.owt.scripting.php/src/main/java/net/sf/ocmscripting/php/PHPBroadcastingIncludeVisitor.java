/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple visitor which broadcasts the events to a collection of sub visitors.
 * <p>
 * 
 * This class is useful if you want the parsing to occur only once but multiple
 * visitors to collect information.
 */
public class PHPBroadcastingIncludeVisitor implements PHPIncludeVisitor {

    /**
     * The list of receiving visitors.
     */
    private List receivers;

    /**
     * Constructs a new instance of this class.
     */
    public PHPBroadcastingIncludeVisitor() {
        initializeMembers();
    }

    /**
     * Initialized the members to sane defaults.
     */
    private void initializeMembers() {
        receivers = new ArrayList();
    }

    /**
     * Add a visitor to the list of event receivers.
     * 
     * @param receiver
     *            The visitor to add
     */
    public void addReceiver(PHPIncludeVisitor receiver) {
        receivers.add(receiver);
    }

    /**
     * Removes a visitor from the list of event receivers.
     * 
     * @param receiver
     *            The visitor to remove
     */
    public void removeReceiver(PHPIncludeVisitor receiver) {
        receivers.remove(receiver);
    }

    /**
     * @see PHPIncludeVisitor#visitIncludeFile(String)
     */
    public void visitIncludeFile(String file) {
        for (Iterator i = receivers.iterator(); i.hasNext();) {
            PHPIncludeVisitor receiver = (PHPIncludeVisitor) i.next();
            receiver.visitIncludeFile(file);
        }
    }

    /**
     * @see PHPIncludeVisitor#visitSurroundingText(String)
     */
    public void visitSurroundingText(String text) {
        for (Iterator i = receivers.iterator(); i.hasNext();) {
            PHPIncludeVisitor receiver = (PHPIncludeVisitor) i.next();
            receiver.visitSurroundingText(text);
        }
    }

}
