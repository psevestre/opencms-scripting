/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsParameterConfiguration;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsPropertyDefinition;
import org.opencms.file.CmsRequestContext;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsVfsResourceNotFoundException;
import org.opencms.file.history.CmsHistoryResourceHandler;
import org.opencms.flex.CmsFlexCache;
import org.opencms.flex.CmsFlexController;
import org.opencms.flex.CmsFlexRequest;
import org.opencms.flex.CmsFlexResponse;
import org.opencms.jsp.util.CmsJspLinkMacroResolver;
import org.opencms.loader.CmsJspLoader;
import org.opencms.loader.I_CmsFlexCacheEnabledLoader;
import org.opencms.loader.I_CmsResourceLoader;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.util.CmsFileUtil;
import org.opencms.util.CmsRequestUtil;
import org.opencms.workplace.CmsWorkplaceManager;

/**
 * This is the base class for a script resource loader.
 * <p>
 * 
 * Base class for custom script resource loaders. This class provided common
 * methods for script resource loaders.
 */
public abstract class CmsScriptLoader implements I_CmsResourceLoader,
        I_CmsFlexCacheEnabledLoader, I_CmsScriptRepositoryBacked {

	/**
	 * Pamameter which allows to output the sourcecode of the file
	 * during static export.
	 */
	public static final String PARAM_EXPORTSOURCE = "export.sourcecode";
	
    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsScriptLoader.class);

    /**
     * The repository used for script files.
     */
    private String m_repositoryRoot;

    /**
     * The configuration of this resource loader.
     */
    private Map m_configuration;

    /**
     * The CmsFlexCache used for storing cached elements.
     */
    private CmsFlexCache m_cache;

    /**
     * Simple Bean no argument exception. Needed so that OpenCms
     * can construct the loader via reflection.
     */
    public CmsScriptLoader() {
    	initMembers();
    }
    
    /**
     * Initialized the members to sane default values.
     */
    protected void initMembers() {
        m_repositoryRoot = null;
        m_cache = null;
        m_configuration = new HashMap();
    }

    /**
     * @see I_CmsResourceLoader#destroy()
     */
    public void destroy() {
        // NOOP (Not really needed)
    }

    /**
     * @see I_CmsResourceLoader#isStaticExportEnabled()
     */
    public boolean isStaticExportEnabled() {
        return true;
    }

    /**
     * @see I_CmsResourceLoader#isStaticExportProcessable()
     */
    public boolean isStaticExportProcessable() {
        return true;
    }

    /**
     * @see I_CmsResourceLoader#isUsableForTemplates()
     */
    public boolean isUsableForTemplates() {
        return false;
    }

    /**
     * @see I_CmsResourceLoader#isUsingUriWhenLoadingTemplate()
     */
    public boolean isUsingUriWhenLoadingTemplate() {
        return false;
    }

    /**
     * Allows adding configuration parameters to this object. This method is
     * used by the OpenCms configuration.
     * 
     * @param paramName
     *            The name of the parameter
     * @param paramValue
     *            The value of the parameter
     */
    public void addConfigurationParameter(String paramName, String paramValue) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.ADDING_CONFIGURATION_PARAM_2, paramName,
                    paramValue));
        }
        m_configuration.put(paramName, paramValue);
    }

    /**
     * Returns the configuration parameters as a unmodifiable Map.
     * 
     * @returns The configuration parameters as a unmodifiable Map.
     */
    public CmsParameterConfiguration getConfiguration() {
        return new CmsParameterConfiguration(Collections.unmodifiableMap(m_configuration));
    }

    /**
     * Sets the CmsFlexCache.
     * 
     * @see I_CmsFlexCacheEnabledLoader
     * @param cache
     *            The CmsFlexCache instance
     */
    public void setFlexCache(CmsFlexCache cache) {
        m_cache = cache;
    }

    /**
     * Sets the RFS path to the repository for the scripts.
     * 
     * @param path
     *            The RFS path to the script repository
     */
    public void setRepository(String path) {
        if (path == null) {
            throw new NullPointerException("Repository path must not be null");
        }
        m_repositoryRoot = path;
    }

    /**
     * Returns the currently configured RFS path to the script repository.
     * 
     * @return The currently configured RFS path to the script repository
     */
    public String getRepository() {
        return m_repositoryRoot;
    }

    /**
     * Extracts the source code from a CmsFile.
     * <p>
     * 
     * @param file
     *            The CmsFile to read
     * @return The source code as a string
     */
    protected String extractSourcecode(CmsFile file) {
        return new String(file.getContents());
    }

    /**
     * Resolves all links for includes.
     * <p>
     * 
     * @param source
     *            The source code
     * @param cms
     *            The CmsObject to use
     * @return The resolved source code
     */
    protected String resolveLinkMacros(String source, CmsObject cms) {
        CmsJspLinkMacroResolver macroResolver = new CmsJspLinkMacroResolver(
                cms, null, true);
        return macroResolver.resolveMacros(source);
    }

    /**
     * Indicates if dispatched requests to a script file are always committed.
     * <p>
     * 
     * This method is intended for overriding it in the extending class. By
     * default this method always returns <code>false</code>
     * 
     * @return <code>true</code> if the request is always committed after
     *         dispatching.
     */
    protected boolean isAlwaysCommitted() {
        return false;
    }

    /**
     * Returns the absolute RFS path to the repository.
     * <p>
     * 
     * @return The absolute RFS path to the repository
     */
    public String getAbsoluteRepository() {
        File repositoryFile = new File(getRepository());
        if (!repositoryFile.isAbsolute()) {
            return OpenCms.getSystemInfo().getAbsoluteRfsPathRelativeToWebInf(
                    getRepository());
        }
        return getRepository();
    }

    /**
     * Purges the repository for this loader.
     * <p>
     * 
     * @see I_CmsScriptRepositoryBacked#purgeRepository()
     */
    public synchronized void purgeRepository() {

        // construct path to the repository and ensure ending slash
        String repository = getRepository();
        if (!repository.endsWith(File.separator)) {
            repository = repository = repository + File.separator;
        }

        File onlineRepository = new File(repository
                + CmsFlexCache.REPOSITORY_ONLINE + File.separator);
        CmsFileUtil.purgeDirectory(onlineRepository);

        File offlineRepository = new File(repository
                + CmsFlexCache.REPOSITORY_OFFLINE + File.separator);
        CmsFileUtil.purgeDirectory(offlineRepository);

    }

    /**
     * Displays the source for the given script resource.
     * <p>
     * 
     * @param cms
     *            The CmsObject to use
     * @param resource
     *            The CmsResource to display the source for
     * @param req
     *            The current request
     * @param res
     *            The current response
     * 
     * @throws CmsException
     *             If any OpenCms operation fails
     * @throws IOException
     *             If any IO operation fails
     */
    protected void displayScriptSource(CmsObject cms, CmsResource resource,
            HttpServletRequest req, HttpServletResponse res)
            throws CmsException, IOException {

        CmsResource scriptResource = (CmsResource) CmsHistoryResourceHandler
                .getHistoryResource(req);
        if (scriptResource == null) {
            scriptResource = resource;
        }

        CmsFile scriptFile = cms.readFile(scriptResource);
        String sourceCode = extractSourcecode(scriptFile);

        res.setContentLength(sourceCode.length());
        res.setContentType("text/plain");
        res.getWriter().write(sourceCode);
    }

    /**
     * Returns the script from request updater for this loader.
     * <p>
     * 
     * @return The script request updater for this loader.
     */
    protected abstract I_ScriptRequestUpdater getScriptUpdater();

    /**
     * Ensure that the RFS copies of the CmsResource and eventually included
     * files are present.
     * <p>
     * 
     * @param cms
     *            The CmsObject for the current request
     * @param resource
     *            The CmsResource to ensure presence for
     * @param rfsPath
     *            The RFS path where the resource should be written
     * 
     * @throws CmsException
     *             If OpenCms operations fail
     * @throws IOException
     *             If RFS operations fail
     */
    protected abstract void ensureRFSCopies(CmsObject cms,
            CmsResource resource, String rfsPath) throws CmsException,
            IOException;

    /**
     * This methods allows extending classes to add custom request attributes
     * during service/dump/export
     * 
     * @param request The request to use
     */
    protected void addRequestAttributes(ServletRequest request) {
    	// NOOP, override in subclass
    }
    
    /**
     * Returns the suffix for this script type.
     * 
     * @return The suffix for this script type
     */
    public abstract String getScriptSuffix();

    /**
     * Service a resource load to a RFS resource.
     * 
     * @see I_CmsResourceLoader#service(CmsObject, CmsResource, ServletRequest,
     *      ServletResponse)
     */
    // This method is a nearly 1:1 copy of the JSP loader
    public void service(CmsObject cms, CmsResource resource,
            ServletRequest req, ServletResponse res) throws ServletException,
            IOException, CmsException {

    	// Allows subclasses to add request attributes
    	addRequestAttributes(req); 	
    	
        CmsFlexController controller = CmsFlexController.getController(req);

        // Compute the RFS path and ensure the suffix
        String rfsPath = CmsScriptingUtil.computeRFSPath(getRepository(), cms,
                resource);
        rfsPath = CmsScriptingUtil.ensureSuffix(rfsPath, getScriptSuffix());

        // Ensure that the RFS versions is up to date
        ensureRFSCopies(cms, resource, rfsPath);

        // Compute the target resource to dispatch to
        // Dispatcher don't likes windows paths because of URL
        String target = CmsScriptingUtil.removeWebappPrefix(rfsPath).replace('\\', '/');

        // TODO: Why only buffering
        controller.getCurrentResponse().setOnlyBuffering(true);
        
        // Now dispatch the request to the external resource
        RequestDispatcher dispatcher = controller.getCurrentRequest()
                .getRequestDispatcherToExternal(cms.getSitePath(resource),
                        target);
        dispatcher.include(req, res);

    }

    /**
     * @see I_CmsResourceLoader#dump(CmsObject, CmsResource, String, Locale,
     *      HttpServletRequest, HttpServletResponse)
     */
    // This method is a nearly 1:1 copy of the JSP loader
    public byte[] dump(CmsObject cms, CmsResource resource, String element,
            Locale locale, HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException, CmsException {
        // TODO: Check if the dump method works correctly

    	// Allows subclasses to add request attributes
    	addRequestAttributes(req);
    	
        // Get the current Flex controller
        CmsFlexController controller = CmsFlexController.getController(req);
        CmsFlexController oldController = null;

        if (controller != null) {
            // For dumping we must create an new "top level" controller, save
            // the old one to be restored later
            oldController = controller;
        }

        byte[] result = null;
        try {
            // now create a new, temporary Flex controller
            controller = getController(cms, resource, req, res, false, false);
            if (element != null) {
                // add the element parameter to the included request
                String[] value = new String[] { element };
                Map parameters = Collections.singletonMap(
                        I_CmsResourceLoader.PARAMETER_ELEMENT, value);
                controller.getCurrentRequest().addParameterMap(parameters);
            }
            // dispatch to the JSP
            result = doScriptDispatch(controller);
            // remove temporary controller
            CmsFlexController.removeController(req);
        } finally {
            if ((oldController != null) && (controller != null)) {
                // update "date last modified"
                oldController.updateDates(controller.getDateLastModified(),
                        controller.getDateExpires());
                // reset saved controller
                CmsFlexController.setController(req, oldController);
            }
        }

        return result;
    }

    /**
     * @see I_CmsResourceLoader#export(CmsObject, CmsResource,
     *      HttpServletRequest, HttpServletResponse)
     */
    // This method is a nearly 1:1 copy of the JSP loader
    public byte[] export(CmsObject cms, CmsResource resource,
            HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException, CmsException {

    	// Allows subclasses to add request attributes
    	addRequestAttributes(req);
    	
    	/*
    	 * Returns the script contents (sourcecode) if configured
    	 * to do so.
    	 */
    	if (isExportSourcecodeEnabled()) {
    		CmsFile scriptFile = cms.readFile(resource);
    		return scriptFile.getContents();
    	}
    	
        // get the Flex controller
        CmsFlexController controller = getController(cms, resource, req, res,
                false, true);

        // dispatch to the Script
        byte[] result = doScriptDispatch(controller);

        // remove the controller from the request
        CmsFlexController.removeController(req);

        // return the contents
        return result;
    }

    /**
     * Tests if the sourcecode should be exported.
     * 
     * @return <code>true</code> If the sourcecode should be exported
     */
    private boolean isExportSourcecodeEnabled() {
    	if (m_configuration != null && m_configuration.get(PARAM_EXPORTSOURCE) != null) {
    		boolean isExportSourcecodeEnabled = Boolean.parseBoolean(
    				(String)m_configuration.get(PARAM_EXPORTSOURCE));
    			return isExportSourcecodeEnabled;
       	}
    	// Return false on default
    	return false;
    }
    
    /**
     * @see I_CmsResourceLoader#load(CmsObject, CmsResource, HttpServletRequest,
     *      HttpServletResponse)
     */
    // This method is a nearly 1:1 copy of the JSP loader
    public void load(CmsObject cms, CmsResource resource,
            HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException, CmsException {

        CmsRequestContext context = cms.getRequestContext();

        // If we load template jsp or template-element jsp (xml contents or xml
        // pages) don't show source (2nd test)
        if ((CmsHistoryResourceHandler.isHistoryRequest(req))
                && (context.getUri().equals(context.removeSiteRoot(resource
                        .getRootPath())))) {
            displayScriptSource(cms, resource, req, res);
        } else {
            // load and process the JSP
            boolean streaming = false;
            boolean bypass = false;

            // read "cache" property for requested VFS resource to check for
            // special "stream" and "bypass" values
            String cacheProperty = cms.readPropertyObject(resource,
                    CmsPropertyDefinition.PROPERTY_CACHE, true).getValue();
            if (cacheProperty != null) {
                cacheProperty = cacheProperty.trim();
                if (CmsJspLoader.CACHE_PROPERTY_STREAM.equals(cacheProperty)) {
                    streaming = true;
                } else if (CmsJspLoader.CACHE_PROPERTY_BYPASS
                        .equals(cacheProperty)) {
                    streaming = true;
                    bypass = true;
                }
            }

            // get the Flex controller (top is always true because we are in the
            // load method)
            CmsFlexController controller = getController(cms, resource, req,
                    res, streaming, true);

            if (bypass || controller.isForwardMode()) {
                // once in forward mode, always in forward mode (for this
                // request)
                controller.setForwardMode(true);
                // bypass Flex cache for this page, update the JSP first if
                // necessary

                // Compute the RFS path and ensure the suffix
                String rfsPath = CmsScriptingUtil.computeRFSPath(
                        getRepository(), cms, resource);
                rfsPath = CmsScriptingUtil.ensureSuffix(rfsPath,
                        getScriptSuffix());

                // Ensure that the RFS versions is up to date
                ensureRFSCopies(cms, resource, rfsPath);

                // Compute the target resource to dispatch to
                String target = CmsScriptingUtil.removeWebappPrefix(rfsPath);

                req.getRequestDispatcher(target).forward(
                        controller.getCurrentRequest(), res);
            } else {
                // Flex cache not bypassed, dispatch to internal JSP
                doScriptDispatch(controller);
            }

            // remove the controller from the request if not forwarding
            if (!controller.isForwardMode()) {
                CmsFlexController.removeController(req);
            }

        }
    }

    /**
     * Performs the actual dispatching to the script resource.
     * <p>
     * 
     * @param controller
     *            The current CmsFlexController
     * 
     * @return The output of the request.
     * 
     * @throws ServletException
     *             If some Servlet error occurs
     * @throws IOException
     *             If an IO operation fails
     */
    // This method is a nearly 1:1 copy of the JSP loader
    private byte[] doScriptDispatch(CmsFlexController controller)
            throws ServletException, IOException {

        // get request / response wrappers
        CmsFlexRequest f_req = controller.getCurrentRequest();
        CmsFlexResponse f_res = controller.getCurrentResponse();

        try {
            f_req.getRequestDispatcher(
                    controller.getCmsObject().getSitePath(
                            controller.getCmsResource())).include(f_req, f_res);
        } catch (SocketException e) {
            // uncritical, might happen if client (browser) does not wait until
            // end of page delivery
            LOG.debug(Messages.get().getBundle().key(
                    Messages.LOG_IGNORING_EXC_1, e.getClass().getName()), e);
        }

        byte[] result = null;
        HttpServletResponse res = controller.getTopResponse();

        if (!controller.isStreaming() && !f_res.isSuspended()) {
            try {
                // if a JSP errorpage was triggered the response will be already
                // committed here
                if (!res.isCommitted() || false || isAlwaysCommitted()) {

                    // check if the current request was done by a workplace user
                    boolean isWorkplaceUser = CmsWorkplaceManager
                            .isWorkplaceUser(f_req);

                    // check if the content was modified since the last request
                    if (controller.isTop()
                            && !isWorkplaceUser
                            && CmsFlexController.isNotModifiedSince(f_req,
                                    controller.getDateLastModified())) {
                        if (f_req.getParameterMap().size() == 0) {
                            // only use "expires" header on pages that have no
                            // parameters,
                            // otherwise some browsers (e.g. IE 6) will not even
                            // try to request
                            // updated versions of the page
                            CmsFlexController.setDateExpiresHeader(res,
                                    controller.getDateExpires(), 0);
                        }
                        res.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                        return null;
                    }

                    // get the result byte array
                    result = f_res.getWriterBytes();
                    HttpServletRequest req = controller.getTopRequest();
                    if (req.getHeader(CmsRequestUtil.HEADER_OPENCMS_EXPORT) != null) {
                        // this is a non "on-demand" static export request,
                        // don't write to the response stream
                        req.setAttribute(CmsRequestUtil.HEADER_OPENCMS_EXPORT,
                                new Long(controller.getDateLastModified()));
                    } else if (controller.isTop()) {
                        // process headers and write output if this is the "top"
                        // request/response
                        res.setContentLength(result.length);
                        // check for preset error code
                        Integer errorCode = (Integer) req
                                .getAttribute(CmsRequestUtil.ATTRIBUTE_ERRORCODE);
                        if (errorCode == null) {
                            // set last modified / no cache headers only if this
                            // is not an error page
                            if (isWorkplaceUser) {
                                res.setDateHeader(
                                        CmsRequestUtil.HEADER_LAST_MODIFIED,
                                        System.currentTimeMillis());
                                CmsRequestUtil.setNoCacheHeaders(res);
                            } else {
                                // set date last modified header
                                CmsFlexController.setDateLastModifiedHeader(
                                        res, controller.getDateLastModified());
                                if ((f_req.getParameterMap().size() == 0)
                                        && (controller.getDateLastModified() > -1)) {
                                    // only use "expires" header on pages that
                                    // have no parameters
                                    // and that are cachable (i.e. 'date last
                                    // modified' is set)
                                    // otherwise some browsers (e.g. IE 6) will
                                    // not even try to request
                                    // updated versions of the page
                                    CmsFlexController.setDateExpiresHeader(res,
                                            controller.getDateExpires(), 0);
                                }
                            }
                            // set response status to "200 - OK" (required for
                            // static export "on-demand")
                            res.setStatus(HttpServletResponse.SC_OK);
                        } else {
                            // set previously saved error code
                            res.setStatus(errorCode.intValue());
                        }
                        // process the headers
                        CmsFlexResponse.processHeaders(f_res.getHeaders(), res);
                        res.getOutputStream().write(result);
                        res.getOutputStream().flush();
                    }
                }
            } catch (IllegalStateException e) {
                // uncritical, might happen if JSP error page was used
                LOG
                        .debug(Messages.get().getBundle().key(
                                Messages.LOG_IGNORING_EXC_1,
                                e.getClass().getName()), e);
            } catch (SocketException e) {
                // uncritical, might happen if client (browser) does not wait
                // until end of page delivery
                LOG
                        .debug(Messages.get().getBundle().key(
                                Messages.LOG_IGNORING_EXC_1,
                                e.getClass().getName()), e);
            }
        }

        return result;
    }

    /**
     * Updates a script resource from a request.
     * <p>
     * 
     * @see org.opencms.loader.CmsJspLoader#updateJspFromRequest(String,
     *      CmsFlexRequest)
     * 
     * @param path
     *            The path to the script
     * @param request
     *            The request to use
     */
    // This method is a nearly 1:1 copy from the JSP integration
    public void updateScriptFromRequest(String path, CmsFlexRequest request) {
        // assemble the RFS name of the requested jsp
        String jspUri = path;
        String pathInfo = request.getPathInfo();
        if (pathInfo != null) {
            jspUri += pathInfo;
        }

        // check the file name
        if ((jspUri == null) || !jspUri.startsWith(getAbsoluteRepository())) {
            // nothing to do, this kind of request are handled by the
            // CmsJspLoader#service method
            return;
        }

        // remove prefixes
        jspUri = jspUri.substring(getAbsoluteRepository().length());
        if (jspUri.startsWith(CmsFlexCache.REPOSITORY_ONLINE)) {
            jspUri = jspUri.substring(CmsFlexCache.REPOSITORY_ONLINE.length());
        } else if (jspUri.startsWith(CmsFlexCache.REPOSITORY_OFFLINE)) {
            jspUri = jspUri.substring(CmsFlexCache.REPOSITORY_OFFLINE.length());
        } else {
            // this is not an OpenCms jsp file
            return;
        }

        // read the resource from OpenCms
        CmsFlexController controller = CmsFlexController.getController(request);
        try {
            CmsResource includeResource;
            try {
                // first try to read the resource assuming no additional jsp
                // extension was needed
                includeResource = CmsScriptingUtil.readScriptResource(
                        controller, jspUri);
            } catch (CmsVfsResourceNotFoundException e) {
                // try removing the additional jsp extension
                if (jspUri.endsWith(getScriptSuffix())) {
                    jspUri = jspUri.substring(0, jspUri.length()
                            - getScriptSuffix().length());
                }
                includeResource = CmsScriptingUtil.readScriptResource(
                        controller, jspUri);
            }
            // make sure the jsp referenced file is generated
            String rfsPath = CmsScriptingUtil.computeRFSPath(getRepository(),
                    controller.getCmsObject(), includeResource);
            rfsPath = CmsScriptingUtil.ensureSuffix(rfsPath, getScriptSuffix());

            ensureRFSCopies(controller.getCmsObject(), includeResource, rfsPath);
        } catch (Exception e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(e.getLocalizedMessage(), e);
            }
        }

    }

    /**
     * Returns the current flex controller or creates a new one.
     * <p>
     * 
     * @param cms
     *            The CmsObject to use
     * @param resource
     *            The requested resource
     * @param req
     *            The current request
     * @param res
     *            The current response
     * @param streaming
     *            If the response should be streamed
     * @param top
     *            If this is the top request (i.e. not included)
     * 
     * @return The created or existing flex controller
     */
    protected CmsFlexController getController(CmsObject cms,
            CmsResource resource, HttpServletRequest req,
            HttpServletResponse res, boolean streaming, boolean top) {

        CmsFlexController controller = null;
        if (top) {
            // only check for existing controller if this is the "top"
            // request/response
            controller = CmsFlexController.getController(req);
        }
        if (controller == null) {
            // create new request / response wrappers
            controller = new CmsFlexController(cms, resource, m_cache, req,
                    res, streaming, top);
            CmsFlexController.setController(req, controller);
            CmsScriptingFlexRequest f_req = new CmsScriptingFlexRequest(req,
                    controller);
            f_req.setScriptUpdater(getScriptUpdater());
            CmsFlexResponse f_res = new CmsFlexResponse(res, controller,
                    streaming, true);
            controller.push(f_req, f_res);
        } else if (controller.isForwardMode()) {
            // reset CmsObject (because of URI) if in forward mode
            controller = new CmsFlexController(cms, controller);
            CmsFlexController.setController(req, controller);
        }
        return controller;
    }

    /**
     * Writes the file contents to the denoted RFS file and eventually creates
     * the file/parents.
     * <p>
     * 
     * @param rfsPath
     *            The RFS path of the file
     * @param contents
     *            The contents for the file
     * 
     * @throws IOException
     *             If writing fails somehow
     */
    protected void writeFile(String rfsPath, String contents)
            throws IOException {

        // Get the handle for the actual file and the parent path
        File rfsFileHandle = new File(rfsPath);
        File rfsParentFileHandle = new File(rfsFileHandle.getParent());

        // If the parent does not exist, create it first
        if (!rfsParentFileHandle.exists()) {
            rfsParentFileHandle.mkdirs();
        }

        // If the file does exist, delete it to clear contents
        if (rfsFileHandle.exists()) {
            rfsFileHandle.delete();
        }

        // Create the file (we deleted it if it was there)
        rfsFileHandle.createNewFile();

        // Write the passed in contents to the file
        FileWriter fw = new FileWriter(rfsFileHandle);
        fw.append(contents);
        fw.close();
    }

}
