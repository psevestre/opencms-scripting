/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting.php;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletRequest;

import net.sf.ocmscripting.CmsScriptLoader;
import net.sf.ocmscripting.CmsScriptingUtil;
import net.sf.ocmscripting.I_ScriptRequestUpdater;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsConfigurationException;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

/**
 * Script loader for the PHP integration.
 * <p>
 */
public class CmsPHPScriptLoader extends CmsScriptLoader implements
        I_ScriptRequestUpdater {

    /**
     * Suffix for PHP script.
     */
    public static final String PHP_SUFFIX = ".php";

    /**
     * The logging channel for this class.
     */
    private static Log LOG = CmsLog.getLog(CmsPHPScriptLoader.class);

    /**
     * The ID for this resource loader.
     */
    public static final int LOADER_ID = 9991;

    /**
     * The variable for the dynamic path translator
     */
	public static final String ATTR_PATH_TRANSLATOR = "__ocmsPathTranslator";

    /**
     * Returns the resource loader ID.
     * 
     * @return The ID of this resource loader
     */
    public int getLoaderId() {
        return LOADER_ID;
    }

    /**
     * Ensures that all RFS copies of the specified resource and their
     * includes/dependencies exist.
     * <p>
     * 
     * @param currentCms
     *            The currently used CmsObject
     * @param file
     *            The script file
     * @param rfsPath
     *            The path of the file in the RFS.
     */
    protected void ensureRFSCopies(CmsObject currentCms, CmsResource file,
            String rfsPath) throws CmsException, IOException {
        CmsObject cms = OpenCms.initCmsObject(currentCms);
        cms.getRequestContext().setSiteRoot("");
        ensureRFSCopies(cms, file, rfsPath, new HashSet());
    }

    /**
     * Recursive implementation of
     * {@link #ensureRFSCopies(CmsObject, CmsResource, String)}.
     * 
     * @see #ensureRFSCopies(CmsObject, CmsResource, String)
     */
    private void ensureRFSCopies(CmsObject cms, CmsResource file,
            String rfsPath, Set alreadyEnsuredResources) throws CmsException,
            IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(Messages.get().getBundle().key(
                    Messages.DEBUG_ENSURE_RFS_COPIES_1, file));
        }
        // The Set ensures all resources are only parsed once

        CmsFile sourceFile = cms.readFile(file);
        String sourceCode = extractSourcecode(sourceFile);
        sourceCode = resolveLinkMacros(sourceCode, cms);
        String currentDirectory = CmsResource.getFolderPath(cms
                .getSitePath(file));

        PHPPathTranslator pathTranslator = new PHPPathTranslator(
                currentDirectory);
        PHPBroadcastingIncludeVisitor broadcastingVisitor = new PHPBroadcastingIncludeVisitor();
        PHPIncludeCollectingVisitor includeCollector = new PHPIncludeCollectingVisitor(
                pathTranslator);
        PHPPathTranslationVisitor contentTranslator = new PHPPathTranslationVisitor(
                pathTranslator, getRepository(), cms);

        broadcastingVisitor.addReceiver(includeCollector);
        broadcastingVisitor.addReceiver(contentTranslator);

        PHPIncludeParser includeParser = new PHPIncludeParser();
        includeParser.parseString(sourceCode, broadcastingVisitor);

        // loop through all includes found in the current file
        for (Iterator i = includeCollector.getIncludedFiles().iterator(); i
                .hasNext();) {
            String currentInclude = (String) i.next();
            
            currentInclude = CmsScriptingUtil.ensureVfsDelimiters(currentInclude);
            
            // If the include was not already parsed earlier
            // and the resource exists. If it not exist we leave
            // it up to the scripting engine to fail.
            if (!alreadyEnsuredResources.contains(currentInclude)
                    && cms.existsResource(currentInclude)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(Messages.get().getBundle().key(
                            Messages.DEBUG_HANDLING_INCLUDE_1, currentInclude));
                }
                CmsResource includedResource = cms.readResource(currentInclude);

                String includeRfsPath = CmsScriptingUtil.computeRFSPath(
                        getRepository(), cms, includedResource);
                includeRfsPath = CmsScriptingUtil.ensureSuffix(includeRfsPath,
                        PHP_SUFFIX);

                // recursivly process the includes of the included resource
                CmsFile includedFile = cms.readFile(includedResource);
                ensureRFSCopies(cms, includedResource, includeRfsPath,
                        alreadyEnsuredResources);
                alreadyEnsuredResources.add(currentInclude);
            }
        }

        if (CmsScriptingUtil.isRFSUpdateNeeded(rfsPath, file)) {
            writeFile(rfsPath, contentTranslator.getTranslatedContents());
        }
    }

    /**
     * @see org.opencms.loader.I_CmsResourceLoader#getResourceLoaderInfo()
     */
    public String getResourceLoaderInfo() {
        return Messages.get().getBundle().key(Messages.RESOURCELOADER_INFO_0);
    }

    /**
     * Initialized the configuration.
     */
    public void initConfiguration() throws CmsConfigurationException {
        setRepository(OpenCms.getSystemInfo()
                .getAbsoluteRfsPathRelativeToWebInf("php"));
    }

    /**
     * Adds the Script Loader helper objects for PHP scripts
     */
	protected void addRequestAttributes(ServletRequest request) {
		super.addRequestAttributes(request);
		request.setAttribute(ATTR_PATH_TRANSLATOR,new CmsPHPDynamicPathTranslator());
	}

	/**
     * @see net.sf.ocmscripting.CmsScriptLoader#getScriptUpdater()
     */
    protected I_ScriptRequestUpdater getScriptUpdater() {
        return this;
    }

    /**
     * @see net.sf.ocmscripting.CmsScriptLoader#getScriptSuffix()
     */
    public String getScriptSuffix() {
        return PHP_SUFFIX;
    }

}
