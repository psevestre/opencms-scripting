/*
 * Id   : $Id$
 * This file is part of the OpenCms scripting language integration.
 * URL: https://sourceforge.net/projects/ocmscripting/ 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package net.sf.ocmscripting;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.opencms.flex.CmsFlexController;
import org.opencms.flex.CmsFlexRequest;

/**
 * Extended CmsFlexRequest for scripting integration.
 * <p>
 * 
 * This class allows updating a script resource from a request. This is a
 * technique used in the normal CmsFlexRequest as a workaround for some Servlet
 * containers.
 * 
 * Since the CmsFlexRequest works only with JSP files the class had to be
 * modularized to allow other scripting language providers to plug in.
 * 
 * @see CmsFlexRequest
 * @see I_ScriptRequestUpdater
 */
public class CmsScriptingFlexRequest extends CmsFlexRequest {

    /**
     * The callback which is used to update a resource.
     */
    private I_ScriptRequestUpdater m_scriptUpdater;

    /**
     * Constructs a new CmsScriptingFlexRequest.
     * 
     * @see CmsFlexRequest#CmsFlexRequest(HttpServletRequest, CmsFlexController)
     * 
     * @param req
     *            The original request
     * @param controller
     *            The CmsFlexController to use
     */
    public CmsScriptingFlexRequest(HttpServletRequest req,
            CmsFlexController controller) {
        super(req, controller);
    }

    /**
     * This is a work around for Servlet containers creating a new application
     * dispatcher instead of using our request dispatcher, so missing RFS JSP
     * pages are not requested to OpenCms and the dispatcher is unable to load
     * the included/forwarded JSP file.
     * <p>
     * 
     * @see javax.servlet.http.HttpServletRequestWrapper#getServletPath()
     */
    public String getServletPath() {

        // unwrap the request to prevent multiple unneeded attempts to generate
        // missing jsp files
        // m_controller.getTopRequest() does not return the right request here
        // when forwarding
        // this method is generally called exactly once per request on different
        // servlet containers
        // only resin calls it twice
        ServletRequest req = getRequest();
        while (req instanceof CmsFlexRequest) {
            req = ((CmsFlexRequest) req).getRequest();
        }
        String servletPath = null;
        if (req instanceof HttpServletRequest) {
            servletPath = ((HttpServletRequest) req).getServletPath();
        } else {
            servletPath = super.getServletPath();
        }
        /*
         * Here we call our callback instead of the JSP loader. This is the only
         * difference to the normal CmsFlexLoader
         */
        m_scriptUpdater.updateScriptFromRequest(servletPath, this);
        return servletPath;
    }

    /**
     * Returns the current {@link I_ScriptRequestUpdater} callback.
     * 
     * @return The current {@link I_ScriptRequestUpdater} callback
     */
    public I_ScriptRequestUpdater getScriptUpdater() {
        return m_scriptUpdater;
    }

    /**
     * Sets the {@link I_ScriptRequestUpdater} callback.
     * 
     * @param updater
     *            The {@link I_ScriptRequestUpdater} callback
     */
    public void setScriptUpdater(I_ScriptRequestUpdater updater) {
        if (updater == null) {
            throw new NullPointerException(
                    "The updater must not be a null pointer");
        }
        m_scriptUpdater = updater;
    }

}
